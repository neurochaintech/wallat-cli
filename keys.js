const elliptic = require("elliptic")
const { hex2b64, hex2str } = require("./utils")

const ecdsa = new elliptic.ec("secp256k1")

const sign = (message, key) => {
    const ecdsaKey = ecdsa.keyFromPrivate(key, "hex")
    const signature = ecdsaKey.sign(message)
    return signature.r.toString("hex", 64) + signature.s.toString("hex", 64)
}
module.exports.sign

module.exports.derivePublicKey = key => {
    const ecdsaKey = ecdsa.keyFromPrivate(key, "hex")
    return ecdsaKey.getPublic().encode("hex")
}

const compressedPublicKey = publicKey => {
    const x = publicKey.x.toString("hex")
    if (publicKey.y.isEven()) {
        return "02" + x
    } else {
        return "03" + x
    }
}
module.exports.compressedPublicKey = compressedPublicKey

module.exports.createKey = () => {
    const key = ecdsa.genKeyPair()
    const exponent = key.getPrivate("hex")
    const publicKey = compressedPublicKey(key.getPublic())
    return {
        private: exponent,
        public: hex2b64(publicKey),
    }
}

module.exports.loadKey = keyData => {
    const privateKey = ecdsa.keyFromPrivate(keyData.private, "hex")
    const publicKey = ecdsa.keyFromPublic(keyData.public, "hex")
    privateKey.pub = publicKey.pub
    return privateKey
}

const hash = message => ecdsa.hash().update(message).digest("hex")
module.exports.hash = hash

const signPayload = (payload, key) => {
    const str = hex2str(payload)
    const digest = hash(str)
    const signature = sign(digest, key)
    return signature
}
module.exports.signPayload = signPayload
