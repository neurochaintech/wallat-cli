const BotApi = require("./bot")
const Keys = require("./keys")
const Transaction = require("./transaction")
const {hex2str} = require("./utils")

async function createAddress() {
    const keys = Keys.createKey()
    const bot = new BotApi(keys.public)
    if (await bot.validate() != 406) {
        return {keys, bot}
    } else {
        return createAddress()
    }
}

(async () => {
    try {
        // const MrExgoKeys = {public: "AnQ7HJqHZYV46juBQEK9cGkZaZC+GivIN2PNrLMrEXGO", private: "468f765bb8019a2f85af7fc80de9549059911a70077efd1cb9d8263206b084f6"}
        const MrExgoKeys = {public: "An80z/xtJ+julvhEjzOvKdT4PKNXhOs8LmlCnSYFa3pZ", private: "b85446cefc55978b3b3f92d493a32d2738ec14c55c86c61f92e149a8a647b8c"}
        const MrExgo = new BotApi(MrExgoKeys.public)
        const other = await createAddress()
        const transaction = new Transaction(other.keys.public, 100, "sending 100 ncc with 1 ncc of fee", 1)
        const id = await MrExgo.send(transaction, MrExgoKeys.private)
        console.log(id)
    } catch(e) {
        console.log(e)
    }
})()

