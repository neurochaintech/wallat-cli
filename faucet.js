const BotApi = require("./bot")
const Keys = require("./keys")
const Transaction = require("./transaction")
const {hex2str} = require("./utils")
const express = require("express")
const app = express()
app.use(express.json({type: "*/*"}))
const PORT = 3000
const NCCAMOUNT = 9001

let registeredKey = {}

const faucetKeys = require("./faucetKeys")

function validateKey(req, res, next) {
    if (req.body.rawData) {
        next()
    } else {
        next("invalid public key")
    }
}

async function sendCoin(req, res, next) {
    try {
        const tr = new Transaction(req.body.rawData, NCCAMOUNT, "💰", 0)
        const fromKey = new BotApi(faucetKeys.public)
        const payload = await fromKey.transaction(tr)
        const str = hex2str(payload)
        const digest = Keys.hash(str)
        const signature = Keys.sign(digest, faucetKeys.private)
        const transaction = await fromKey.sendTransaction(payload, signature)
        res.send(transaction.id)
    } catch (e) {
        next(e)
    }
}

app.post("/", validateKey, sendCoin)
app.use((err, req, res, next) => {
    res.status(403)
    res.send(err)
})
app.listen(PORT, () => console.log(`faucet listening on ${PORT}!`))
